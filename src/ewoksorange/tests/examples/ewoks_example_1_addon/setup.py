from ewoksorange import __version__
from ewoksorange.setuptools import setup

if __name__ == "__main__":
    setup(
        __file__,
        name="ewoks-example-1-addon",
        version=__version__,
        description="Ewoks example widgets: addon 1",
    )
